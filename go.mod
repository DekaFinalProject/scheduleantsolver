module gitlab.com/DekaFinalProject/scheduleantsolver

go 1.16

require (
	gitlab.com/DekaFinalProject/dekaants v0.0.5-0.20210519112413-61c041d90c82
	gitlab.com/DekaFinalProject/schedulelib v0.0.6-0.20210519111642-eaa91ea2772d
)
