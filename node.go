package scheduleantsolver

import (
	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type node struct {
	ev schedule.Event
}

func newNode(event schedule.Event) dekaants.Node {
	return &node{
		ev: event,
	}
}

func (n node) ID() dekaants.Identifier {
	return dekaants.Identifier(n.ev.ID())
}
