package scheduleantsolver

import "gitlab.com/DekaFinalProject/dekaants"

type scheduleAntFactory struct {
	edgeChooseConfig ProportionalChooseStrategyConfig
	solverStrategy   MaxMinStrategyConfig
}

func NewScheduleAntFactory(ecc ProportionalChooseStrategyConfig, ss MaxMinStrategyConfig) dekaants.Factory {
	return &scheduleAntFactory{
		edgeChooseConfig: ecc,
		solverStrategy:   ss,
	}
}

func (f scheduleAntFactory) CreateAnswer() dekaants.Answer {
	return newAnswer()
}

func (f scheduleAntFactory) CreateEdgeChooseStrategy() dekaants.EdgeChooseStrategy {
	return NewProportionalChooseStrategy(f.edgeChooseConfig)
}

func (f scheduleAntFactory) CreateSolverStrategy() dekaants.SolverStrategy {
	return NewMaxMinStrategy(f.solverStrategy)
}
