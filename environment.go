package scheduleantsolver

import (
	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type scheduleEnvironment struct {
	graph     schedule.AOEGraph           // Graph of a problem
	resources schedule.ResourceCollection // Resources of a problem

	// Experimental

	edgesFromSlice  [][]dekaants.Edge                             // Holds edges from specified node
	edgesToSlice    [][]dekaants.Edge                             // Holds edges to specified nod
	taskConnections map[schedule.Identifier][]dekaants.Identifier // Holds identifiers of nodes where tasks edges end

	nodesCount int // Count of nodes
	tasksCount int // Count of tasks(Not task edges)

	// Old
	// edgesFrom       map[dekaants.Identifier][]dekaants.Edge
	// edgesTo         map[dekaants.Identifier][]dekaants.Edge
	// taskConnections map[schedule.Identifier][]dekaants.Identifier
	// reverseEdges    map[dekaants.Identifier]map[dekaants.Identifier]dekaants.Edge

	// Lazy init
	lazyEdges []dekaants.Edge // all edges
}

func NewScheduleEnvironment(problem schedule.Problem) dekaants.Environment {
	var out scheduleEnvironment
	out.graph = problem.Graph()
	out.resources = problem.Resources()

	out.nodesCount = len(out.graph.EventsIDs())
	out.tasksCount = len(out.graph.TasksIDs())

	// Slices init
	out.edgesFromSlice = make([][]dekaants.Edge, out.nodesCount)
	out.edgesToSlice = make([][]dekaants.Edge, out.nodesCount)

	// generate taskEdges
	var ids = out.graph.EventsIDs()
	for _, id := range ids {
		var tasksEdges = out.graph.TasksFrom(id)
		for _, task := range tasksEdges {
			out.addEdgeFrom(dekaants.Identifier(id), newTaskEdge(task))
		}
	}
	// Generate task connection
	out.taskConnections = make(map[schedule.Identifier][]dekaants.Identifier, out.tasksCount)
	out.foreachTaskEdge(func(te *taskEdge) {
		var taskID = te.connectedEdge.Task().ID()
		out.addTaskConnection(taskID, te.To().ID())
	})

	// Generate reverse connections
	for _, e := range out.Edges() {
		out.addEdgeTo(e.To().ID(), e)
	}

	return &out
}

func (e scheduleEnvironment) NodesIDs() []dekaants.Identifier {
	var ids = e.graph.EventsIDs()
	var converted = make([]dekaants.Identifier, len(ids))
	for i := range ids {
		converted[i] = dekaants.Identifier(ids[i])
	}

	return converted
}

func (e scheduleEnvironment) Node(id dekaants.Identifier) dekaants.Node {
	return newNode(e.graph.Event(schedule.Identifier(id)))
}

func (e scheduleEnvironment) Edges() []dekaants.Edge {
	if e.lazyEdges == nil {
		var out = make([]dekaants.Edge, 0)
		for _, edges := range e.edgesFromSlice {
			out = append(out, edges...)
		}
		e.lazyEdges = out
	}

	return e.lazyEdges
}

func (e scheduleEnvironment) From(nodeID dekaants.Identifier) []dekaants.Edge {
	return e.getEdgesFrom(nodeID)
}

func (e scheduleEnvironment) StartNode() dekaants.Node {
	return newNode(e.graph.StartEvent())
}

func (e scheduleEnvironment) EndNode() dekaants.Node {
	return newNode(e.graph.EndEvent())
}

func (e scheduleEnvironment) foreachTaskEdge(processFunc func(*taskEdge)) {
	var edges = e.Edges()
	for _, e := range edges {
		if taskEdge, ok := e.(*taskEdge); ok {
			processFunc(taskEdge)
		}
	}
}

func (e *scheduleEnvironment) addEdgeFrom(nodeID dekaants.Identifier, edge dekaants.Edge) {
	if nodeID != edge.From().ID() {
		panic("Incorrect adding edge")
	}

	if e.edgesFromSlice[nodeID] == nil {
		e.edgesFromSlice[nodeID] = make([]dekaants.Edge, 0)
	}

	e.edgesFromSlice[nodeID] = append(e.edgesFromSlice[nodeID], edge)
}

func (e scheduleEnvironment) getEdgesFrom(nodeID dekaants.Identifier) []dekaants.Edge {
	if e.edgesFromSlice[nodeID] == nil {
		e.edgesFromSlice[nodeID] = make([]dekaants.Edge, 0)
	}

	return e.edgesFromSlice[nodeID]
}

func (e *scheduleEnvironment) addEdgeTo(nodeID dekaants.Identifier, edge dekaants.Edge) {
	if nodeID != edge.To().ID() {
		panic("Incorrect adding edge")
	}

	if e.edgesToSlice[nodeID] == nil {
		e.edgesToSlice[nodeID] = make([]dekaants.Edge, 0)
	}

	e.edgesToSlice[nodeID] = append(e.edgesToSlice[nodeID], edge)
}

func (e scheduleEnvironment) getEdgesTo(nodeID dekaants.Identifier) []dekaants.Edge {
	if e.edgesToSlice[nodeID] == nil {
		e.edgesToSlice[nodeID] = make([]dekaants.Edge, 0)
	}

	return e.edgesToSlice[nodeID]
}

func (e *scheduleEnvironment) addTaskConnection(taskID schedule.Identifier, nodeID dekaants.Identifier) {
	slice, ok := e.taskConnections[taskID]
	if !ok || slice == nil {
		e.taskConnections[taskID] = make([]dekaants.Identifier, 0)
	}

	e.taskConnections[taskID] = append(e.taskConnections[taskID], nodeID)

}

func (e scheduleEnvironment) getTaskConnectionsTo(taskID schedule.Identifier) []dekaants.Identifier {
	slice, ok := e.taskConnections[taskID]
	if !ok || slice == nil {
		e.taskConnections[taskID] = make([]dekaants.Identifier, 0)
	}

	return e.taskConnections[taskID]
}
