package scheduleantsolver

import (
	"math"

	"gitlab.com/DekaFinalProject/dekaants"
)

type MaxMinStrategyConfig struct {
	MinMark       float64 ///
	MaxMark       float64
	EvaporateCoff float64

	EliteCount   int
	EliteDivider int

	RangeCoff     float64
	SmoothingCoff float64
}

func MakeMaxMinStrategyConfig() MaxMinStrategyConfig {
	return MaxMinStrategyConfig{
		MinMark:       0.01,
		MaxMark:       1,
		EvaporateCoff: 0.2,

		EliteCount:   5,
		EliteDivider: 5,

		RangeCoff:     0.5,
		SmoothingCoff: 0.1,
	}
}

type maxMinStrategy struct {
	cfg MaxMinStrategyConfig

	environment   *scheduleEnvironment
	iterationBest dekaants.Answer
	globalBest    dekaants.Answer

	iteration uint64
}

func NewMaxMinStrategy(cfg MaxMinStrategyConfig) dekaants.SolverStrategy {
	var out maxMinStrategy
	out.cfg = cfg
	out.environment = nil

	return &out
}

func (s *maxMinStrategy) SetEnvironment(env dekaants.Environment) {
	s.environment = env.(*scheduleEnvironment)
}

func (s *maxMinStrategy) InitEdges() {
	var edges = s.environment.Edges()
	for _, e := range edges {
		e.SetMark(dekaants.MarkValue(s.cfg.MaxMark))
	}
}

func (s *maxMinStrategy) SetIteration(iterationNumber uint64) {
	s.iteration = iterationNumber
}

func (s *maxMinStrategy) SetIterationBestAnswer(answer dekaants.Answer) {
	s.iterationBest = answer
}

func (s *maxMinStrategy) SetGlobalBestAnswer(answer dekaants.Answer) {
	s.globalBest = answer
}

func (s *maxMinStrategy) UpdateMark() {
	// Adaptive limit changes
	s.cfg.MaxMark = (1 / (s.cfg.EvaporateCoff)) * (1 / s.globalBest.Metric())
	var graphS = float64(len(s.environment.NodesIDs()))
	var rangeS = math.Pow(s.cfg.RangeCoff, 1/graphS)

	var fullFactor = 0.0
	for _, n := range s.environment.NodesIDs() {
		fullFactor += s.branchFactor(n, 0.05)
	}
	var avg = fullFactor / graphS
	s.cfg.MinMark = (s.cfg.MaxMark * (1 - rangeS)) / ((avg - 1) * rangeS)
	if avg < 1 {
		// Branching factor is very low. Apply smoothing
		var edges = s.environment.Edges()
		for _, e := range edges {
			var m = e.Mark()
			m = m + dekaants.MarkValue(s.cfg.SmoothingCoff*(s.cfg.MaxMark-float64(m))) // Smoothing
			e.SetMark(m)
		}
	}

	if s.cfg.MaxMark < s.cfg.MinMark {
		s.cfg.MinMark = s.cfg.MaxMark
	}

	// Evaporate
	// var edges = s.environment.Edges()
	// for _, e := range edges {
	// 	var m = e.Mark()
	// 	m *= dekaants.MarkValue(1 - s.cfg.EvaporateCoff) // Evaporate
	// 	m = s.clamp(m, dekaants.MarkValue(s.cfg.MinMark), dekaants.MarkValue(s.cfg.MaxMark))
	// 	e.SetMark(m)
	// }
	// Marking
	var itBestEdges = s.iterationBest.Edges()
	var solMetric = s.iterationBest.Metric()
	var multiplier = 1
	if s.iteration%uint64(s.cfg.EliteDivider) == 0 { // Each s.cfg.EliteDivider iteration use global
		itBestEdges = s.globalBest.Edges()
		solMetric = s.globalBest.Metric()
		multiplier = s.cfg.EliteCount
	}
	for _, e := range itBestEdges {
		var m = e.Mark()
		m *= dekaants.MarkValue(1 - s.cfg.EvaporateCoff) // Evaporate
		m += dekaants.MarkValue(1.0/solMetric) * dekaants.MarkValue(multiplier)
		m = s.clamp(m, dekaants.MarkValue(s.cfg.MinMark), dekaants.MarkValue(s.cfg.MaxMark))
		e.SetMark(m)
	}

}

func (s *maxMinStrategy) clamp(val, min, max dekaants.MarkValue) dekaants.MarkValue {
	if val < min {
		return min
	} else if max < val {
		return max
	}
	return val
}

func (s maxMinStrategy) branchFactor(eID dekaants.Identifier, lambda dekaants.MarkValue) float64 {
	var edges = s.environment.From(eID)
	if len(edges) == 0 {
		return 0
	}
	var edgeMin, edgeMax = s.edgeWithMinMark(edges), s.edgeWithMaxMark(edges)

	var delta = edgeMax.Mark() - edgeMin.Mark()
	var count = 0.0
	for _, e := range edges {
		if e.Mark() > lambda*delta+edgeMin.Mark() {
			count++
		}
	}

	return count
}

func (s maxMinStrategy) edgeWithMinMark(edges []dekaants.Edge) dekaants.Edge {
	if len(edges) == 0 {
		return nil
	}

	var minEdge = edges[0]
	var minMark dekaants.MarkValue = minEdge.Mark()
	for _, e := range edges {
		if e.Mark() < minMark {
			minMark = e.Mark()
			minEdge = e
		}
	}

	return minEdge
}

func (s maxMinStrategy) edgeWithMaxMark(edges []dekaants.Edge) dekaants.Edge {
	if len(edges) == 0 {
		return nil
	}

	var maxEdge = edges[0]
	var maxMark dekaants.MarkValue = maxEdge.Mark()
	for _, e := range edges {
		if maxMark < e.Mark() {
			maxMark = e.Mark()
			maxEdge = e
		}
	}

	return maxEdge
}
