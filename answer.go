package scheduleantsolver

import (
	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
	"gitlab.com/DekaFinalProject/schedulelib/schedule/simple"
)

type ScheduleAnswer interface {
	Schedule() schedule.Schedule
}

type answer struct {
	// Out
	schedule schedule.Schedule
	err      error

	// Condition
	env           dekaants.Environment
	resourcesInit schedule.ResourceCollection

	// SolutionBuilding vars
	currentNodes []dekaants.Node
	edges        []dekaants.Edge
	allowedEdges []dekaants.Edge

	resources schedule.ResourceCollection

	completedTasksIDs []dekaants.Identifier
	eventsTimes       map[dekaants.Identifier]schedule.Time
}

func newAnswer() *answer {
	var out answer

	out.env = nil
	out.currentNodes = make([]dekaants.Node, 0)
	out.edges = make([]dekaants.Edge, 0)

	out.resourcesInit = nil
	out.resources = out.resourcesInit

	out.Reset()
	return &out
}

func (a answer) Schedule() schedule.Schedule {
	return a.schedule
}

func (a *answer) AddEdge(edge dekaants.Edge) {
	if a.env == nil {
		panic("cannot build answer without environment")
	}

	switch e := edge.(type) {
	case *taskEdge:
		a.addTaskEdge(e)
	default:
		panic("unsupported edge type")
	}
}

func (a answer) CurrentNodes() []dekaants.Node {
	return a.currentNodes
}

func (a answer) Edges() []dekaants.Edge {
	return a.edges
}

func (a answer) Metric() float64 {
	return float64(a.schedule.FullDuration())
}

func (a answer) Completed() bool {
	if a.env == nil {
		return false
	}
	if a.err != nil {
		return true
	}

	var endEventID = a.env.EndNode().ID()
	_, ok := a.eventsTimes[endEventID]
	return ok
}

func (a *answer) SetEnvironment(env dekaants.Environment) {
	a.env = env
	convertedEnv, ok := a.env.(*scheduleEnvironment)
	if !ok {
		panic("unsupported environment")
	}
	a.resourcesInit = convertedEnv.resources.Copy().(schedule.ResourceCollection)
	a.schedule = simple.NewFactory().NewSchedule()
	// Add tasks
	for _, taskIDs := range convertedEnv.graph.TasksIDs() {
		var task = convertedEnv.graph.Task(taskIDs)
		a.schedule.AddTasks(task)
	}

	a.Reset()
}

func (a *answer) Reset() {
	if a.env == nil {
		return
	}

	// Reset schedule
	convertedEnv, ok := a.env.(*scheduleEnvironment)
	if !ok {
		panic("unsupported environment")
	}
	a.schedule = simple.NewFactory().NewSchedule()
	// Add tasks
	for _, taskIDs := range convertedEnv.graph.TasksIDs() {
		var task = convertedEnv.graph.Task(taskIDs)
		a.schedule.AddTasks(task)
	}

	a.currentNodes = make([]dekaants.Node, 0)
	a.currentNodes = append(a.currentNodes, a.env.StartNode())
	var startEdges = a.env.From(a.env.StartNode().ID())
	a.allowedEdges = make([]dekaants.Edge, len(startEdges))
	copy(a.allowedEdges, startEdges)

	a.resources = a.resourcesInit.Copy().(schedule.ResourceCollection)

	a.edges = make([]dekaants.Edge, 0)

	a.completedTasksIDs = make([]dekaants.Identifier, 0)
	a.eventsTimes = make(map[dekaants.Identifier]schedule.Time)
	a.eventsTimes[a.env.StartNode().ID()] = 0
}

func (a *answer) Error() error {
	return a.err
}

func (a *answer) Copy() dekaants.Answer {
	var out = newAnswer()
	// Copy schedule
	out.schedule = simple.NewFactory().NewSchedule()
	for _, tID := range a.schedule.TaskIDs() {
		out.schedule.AddTasks(a.schedule.Task(tID))
		taskTime, _ := a.schedule.TaskStartTime(tID)
		out.schedule.SetTaskStartTime(tID, taskTime)
	}

	out.env = a.env
	out.resourcesInit = a.resourcesInit
	out.err = a.err

	out.edges = make([]dekaants.Edge, len(a.edges))
	copy(out.edges, a.edges)

	return out
}

func (a *answer) addTaskEdge(edge *taskEdge) {
	// Acquire resource and get time of start
	var task = edge.connectedEdge.Task()
	minTime, ok := a.eventsTimes[edge.From().ID()]
	if !ok {
		panic("trying to complete task without previous completed")
	}
	realTime, err := task.AcquireResources(a.resources, minTime, task.Effort())
	if err != nil {
		a.err = err
		return
	}
	// Add task to schedule and to completed tasks
	a.schedule.SetTaskStartTime(task.ID(), realTime)
	a.completedTasksIDs = append(a.completedTasksIDs, dekaants.Identifier(task.ID()))
	a.edges = append(a.edges, edge)
	a.currentNodes = append(a.currentNodes, edge.To())

	// Check if event is completed if yes add and set time in eventTimes
	convertedEnv := a.env.(*scheduleEnvironment)
	toUpdate := convertedEnv.getTaskConnectionsTo(task.ID())
	for _, eventIDs := range toUpdate {
		a.updateEventComplete(eventIDs)

		// Add new edges only if event completed
		if a.eventCompleted(eventIDs) {
			a.allowedEdges = append(a.allowedEdges, a.env.From(eventIDs)...)
		}
	}

	a.clearAllowedEdges(dekaants.Identifier(task.ID()))
}

func (a *answer) updateEventComplete(id dekaants.Identifier) {
	convertedEnv, ok := a.env.(*scheduleEnvironment)
	if !ok {
		panic("unsupported environment")
	}

	var edges = convertedEnv.getEdgesTo(id)

	var allCompleted = true
	var maxTime schedule.Time = 0

	for _, e := range edges {
		if taskE, ok := e.(*taskEdge); ok { //Check only taskEdges
			var task = taskE.connectedEdge.Task()

			if !a.taskCompleted(dekaants.Identifier(task.ID())) {
				allCompleted = false
				break
			}

			var taskTime, ok = a.schedule.TaskStartTime(task.ID())
			if !ok {
				panic("internal error")
			}

			if maxTime < taskTime+task.Effort() {
				maxTime = taskTime + task.Effort()
			}

		}
	}

	if allCompleted {
		a.eventsTimes[id] = maxTime
	}
}

func (a *answer) taskCompleted(id dekaants.Identifier) bool {
	for _, tID := range a.completedTasksIDs {
		if tID == id {
			return true
		}
	}
	return false
}

func (a *answer) eventCompleted(id dekaants.Identifier) bool {
	_, ok := a.eventsTimes[id]
	return ok
}

func (a *answer) clearAllowedEdges(targetTaskID dekaants.Identifier) {
	var newElements = make([]dekaants.Edge, 0, len(a.allowedEdges))

	for pos := 0; pos < len(a.allowedEdges); pos++ {
		var e2 = a.allowedEdges[pos].(*taskEdge)
		if e2.connectedEdge.Task().ID() != schedule.Identifier(targetTaskID) {
			newElements = append(newElements, e2)
		}
	}

	a.allowedEdges = newElements
}
