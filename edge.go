package scheduleantsolver

import (
	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type taskEdge struct {
	connectedEdge schedule.TaskEdge
	mark          dekaants.MarkValue

	start dekaants.Node
	end   dekaants.Node
}

func newTaskEdge(connectedEdge schedule.TaskEdge) dekaants.Edge {
	var out taskEdge
	out.connectedEdge = connectedEdge
	out.mark = 1

	out.start = newNode(out.connectedEdge.StartEvent())
	out.end = newNode(out.connectedEdge.EndEvent())
	return &out
}

func (e taskEdge) From() dekaants.Node {
	return e.start
}

func (e taskEdge) To() dekaants.Node {
	return e.end
}

func (e *taskEdge) SetMark(mark dekaants.MarkValue) {
	e.mark = mark
}

func (e taskEdge) Mark() dekaants.MarkValue {
	return e.mark
}

func (e taskEdge) Metric() float64 {
	return float64(e.connectedEdge.Task().Effort())
}
