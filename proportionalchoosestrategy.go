package scheduleantsolver

import (
	"math"
	"math/rand"
	"time"

	"gitlab.com/DekaFinalProject/dekaants"
	"gitlab.com/DekaFinalProject/schedulelib/schedule"
)

type ProportionalChooseStrategyConfig struct {
	R0coff     float64
	MarkCoff   float64
	MetricCoff float64
}

func MakeProportionalChooseStrategyConfig() ProportionalChooseStrategyConfig {
	return ProportionalChooseStrategyConfig{
		R0coff:     0.9,
		MarkCoff:   2,
		MetricCoff: 1,
	}
}

type proportionalChooseStrategy struct {
	cfg ProportionalChooseStrategyConfig

	env dekaants.Environment
	rng *rand.Rand
}

func NewProportionalChooseStrategy(cfg ProportionalChooseStrategyConfig) dekaants.EdgeChooseStrategy {
	var out proportionalChooseStrategy
	out.cfg = cfg
	out.rng = rand.New(rand.NewSource(time.Now().UnixNano()))

	return &out
}

func (s *proportionalChooseStrategy) SetEnvironment(env dekaants.Environment) {
	s.env = env
}

func (s *proportionalChooseStrategy) ChooseEdge(ans dekaants.Answer) dekaants.Edge {
	if s.env == nil {
		return nil
	}

	var convertedAnswer = ans.(*answer)
	var nodes = convertedAnswer.allowedEdges
	var filteredEdges = s.filterEdges(convertedAnswer, nodes)
	// Now we must choose from filtered edges exact one
	return s.chooseFrom(convertedAnswer, filteredEdges)

}

func (s *proportionalChooseStrategy) filterEdges(ans *answer, edges []dekaants.Edge) []dekaants.Edge {
	var out = make([]dekaants.Edge, 0)
	// Allow events with completed deps
	for _, e := range edges {
		var eConv = e.(*taskEdge)
		if ans.eventCompleted(e.From().ID()) {
			if !ans.taskCompleted(dekaants.Identifier(eConv.connectedEdge.Task().ID())) {
				out = append(out, e)
			}
		}
	}
	return out
}

func (s *proportionalChooseStrategy) chooseFrom(ans *answer, edges []dekaants.Edge) dekaants.Edge {
	if len(edges) == 0 {
		return nil
	}
	if len(edges) == 1 {
		return edges[0]
	}

	var maxTaskEdge = s.taskEdgeWithMaxEndTime(ans, edges)
	var edgeHeuristic = func(e dekaants.Edge) float64 {
		if maxTaskEdge == nil {
			return 1
		}
		var startTime = ans.eventsTimes[e.From().ID()]
		var maxTime = startTime + schedule.Time(maxTaskEdge.Metric())
		var edgeTime = startTime + schedule.Time(e.Metric())
		return math.Pow(float64(maxTime)-float64(edgeTime)+1, s.cfg.MetricCoff)
	}

	var edgeValue = func(e dekaants.Edge) dekaants.MarkValue {
		var val1 = math.Pow(float64(e.Mark()), s.cfg.MarkCoff)
		var val2 = edgeHeuristic(e)
		var out = val1 * val2
		return dekaants.MarkValue(out)
	}

	// Choose max or random
	r := s.rng.Float64()
	if r <= s.cfg.R0coff {
		//Choose max
		return s.edgeWithMaxValue(edges, edgeValue)
	}

	// Choose random
	var N = len(edges)
	var sum = s.sumEdgesValue(edges, edgeValue)

	var mapping = make([]dekaants.MarkValue, N)
	mapping[0] = edgeValue(edges[0]) / sum
	for i := 1; i < N; i++ {
		mapping[i] = mapping[i-1] + edgeValue(edges[i])/sum
	}

	// Get random by value from 0 to 1
	var value = dekaants.MarkValue(s.rng.Float64())
	if value <= mapping[0] {
		return edges[0]
	}
	for i := 1; i < len(edges); i++ {
		if mapping[i-1] < value && value <= mapping[i] {
			return edges[i]
		}
	}
	return edges[len(edges)-1]
}

func (s proportionalChooseStrategy) edgeWithMaxValue(edges []dekaants.Edge, edgeValueFunc func(e dekaants.Edge) dekaants.MarkValue) dekaants.Edge {
	var maxEdge = edges[0]
	var maxVal = edgeValueFunc(maxEdge)

	for _, e := range edges {
		var v = edgeValueFunc(e)
		if maxVal < v {
			maxVal = v
			maxEdge = e
		}
	}

	return maxEdge
}

func (s proportionalChooseStrategy) taskEdgeWithMaxEndTime(ans *answer, edges []dekaants.Edge) dekaants.Edge {

	// Find first taskEdge
	var pos = 0
	for pos = 0; pos < len(edges); pos++ {
		switch edges[pos].(type) {
		case *taskEdge:
			break
		}
	}
	if pos == len(edges) {
		return nil
	}

	var maxEdge dekaants.Edge = edges[pos]
	var maxVal = ans.eventsTimes[maxEdge.From().ID()] + schedule.Time(maxEdge.Metric())

	for ; pos < len(edges); pos++ {
		switch convEdge := edges[pos].(type) {
		case *taskEdge:
			var v = ans.eventsTimes[convEdge.From().ID()] + schedule.Time(convEdge.Metric())
			if maxVal < v {
				maxVal = v
				maxEdge = edges[pos]
			}
		}

	}

	return maxEdge
}

func (s proportionalChooseStrategy) sumEdgesValue(edges []dekaants.Edge, edgeValueFunc func(e dekaants.Edge) dekaants.MarkValue) dekaants.MarkValue {
	var sum dekaants.MarkValue = 0

	for _, e := range edges {
		sum += edgeValueFunc(e)
	}

	return sum
}
